import pytest

from launchpad_seq.scale import Scale, ScaleBimap, scale_to_chromatic, chromatic_to_scale

scale_to_chromatic_data = [
    ((Scale.MAJOR, range(15), 0), (0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24)),
    ((Scale.MAJOR, (x + 2 for x in range(15)), 0), (4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24, 26, 28)),
    ((Scale.MAJOR, range(15), 4), (4, 6, 8, 9, 11, 13, 15, 16, 18, 20, 21, 23, 25, 27, 28)),
]


@pytest.mark.parametrize("params, expected", scale_to_chromatic_data)
def test_scale_to_chromatic(params, expected):
    scale, notes, root = params
    bimap = ScaleBimap(Scale.MAJOR)
    out_notes = tuple(scale_to_chromatic(in_note, bimap, root) for in_note in notes)

    assert out_notes == expected


chromatic_to_scale_data = [
    ((Scale.MAJOR, (0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24), 0), tuple(range(15))),
    ((Scale.MAJOR, (8, 10, 12, 13, 15, 17, 19, 20, 22, 24, 25, 27, 29, 31, 32), 8), tuple(range(15))),
    ((Scale.MAJOR, (5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24, 26, 28, 29), 0), tuple(range(3, 18))),
    ((Scale.MAJOR, (1, 3, 4, 6, 8, 9, 11, 13, 14, 16, 18, 19, 21, 23, 25), 0), tuple(range(15))),
]


@pytest.mark.parametrize("params, expected", chromatic_to_scale_data)
def test_chromatic_to_scale(params, expected):
    scale, notes, root = params
    bimap = ScaleBimap(scale)
    out_notes = tuple(chromatic_to_scale(in_note, bimap, root) for in_note in notes)

    assert out_notes == expected
