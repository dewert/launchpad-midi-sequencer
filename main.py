# coding: utf-8

import asyncio
import logging

from typing import List

import mido
from aiostream import stream

from launchpad_seq.application import SequencerApplication
from launchpad_seq.launchpad import press_stream, GridValue
from launchpad_seq.hardware import ClockEvent


async def drive_sequencer(application: SequencerApplication):
    ports = application.ports
    note_on_events: List[mido.Message] = []

    all_streams = stream.merge(
        press_stream(application.ports.launchpad_input_port),
        application.clock_stream()
    )

    async with all_streams.stream() as streamer:
        async for event in streamer:
            if isinstance(event, GridValue):
                application.controller.handle_press(event)
                continue

            elif isinstance(event, ClockEvent):
                if event == ClockEvent.CLOCK:
                    pass
                elif event == ClockEvent.RESET:
                    application.sequencer.time_reset()
                    continue
                elif event == ClockEvent.START:
                    pass
                else:
                    continue

            application.controller.update_step()

            for msg in note_on_events:
                logger.debug("Note off: %d", msg.note)
                ports.note_output_port.send(
                    mido.Message("note_off", note=msg.note, velocity=0, channel=msg.channel)
                )

            note_on_events = []

            for step in application.sequencer.clock_steps():
                logger.debug(
                    "Track: %d, Gate: %s, Note: %d", step.track, step.gate, step.note.value
                )
                note_on_events.append(
                    mido.Message(
                        "note_on",
                        note=step.note.value,
                        velocity=127,
                        channel=application.config.channels[step.track],
                    )
                )

            for msg in note_on_events:
                ports.note_output_port.send(msg)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(levelname)s][%(name)s] %(message)s (%(filename)s:%(lineno)d)",
        handlers=[logging.StreamHandler()],
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    logger = logging.getLogger("::main::")
    logger.setLevel(logging.DEBUG)

    logger.info("Available MIDI inputs: %s", mido.get_input_names())
    logger.info("Available MIDI outputs: %s", mido.get_output_names())

    sequencer_application = SequencerApplication("config.ini")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(drive_sequencer(sequencer_application))
    loop.close()

