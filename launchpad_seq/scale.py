# coding: utf-8

from enum import Enum
from typing import Dict, List, Union

CHROM_MIN_NOTE = 0
CHROM_MAX_NOTE = 11


class Scale(Enum):
    CHROMATIC = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    MAJOR = [0, 2, 4, 5, 7, 9, 11]
    MINOR = [0, 2, 3, 5, 7, 8, 10]
    MIXOLYDIAN = [0, 2, 4, 5, 7, 9, 10]
    BLUES = [0, 3, 5, 6, 7, 10]


def _find_closest(chromatic_note, c2s_map):
    assert len(c2s_map) <= 12
    assert any(k in range(12) for k in c2s_map.keys())

    tried = set()
    radius = 0

    while len(c2s_map) > len(tried):
        higher = chromatic_note + radius
        lower = chromatic_note - radius

        if lower not in tried and lower >= CHROM_MIN_NOTE:
            try:
                return c2s_map[lower]
            except KeyError:
                tried.add(lower)
        if higher not in tried and higher <= CHROM_MAX_NOTE:
            try:
                return c2s_map[higher]
            except KeyError:
                tried.add(higher)

        radius += 1


class ScaleBimap:
    def __init__(self, scale: Union[Scale, List[int]]):
        scale_value = scale.value if isinstance(scale, Scale) else scale
        self.s2c = {k: v for k, v in enumerate(scale_value)}
        self.c2s = {v: k for k, v in self.s2c.items()}
        self._fill_c2s()

    def _fill_c2s(self):
        added_lookup = {}
        for c in range(12):
            if c in self.c2s:
                continue
            added_lookup[c] = _find_closest(c, self.c2s)

        self.c2s.update(added_lookup)

    def __len__(self):
        return len(self.s2c)


def chromatic_to_scale(chromatic_note, scale_bimap, root_c):
    degree_c = (chromatic_note - root_c) % 12
    degree_s = scale_bimap.c2s[degree_c]
    return len(scale_bimap) * ((chromatic_note - root_c) // 12) + degree_s


def scale_to_chromatic(scale_note, scale_bimap, root_c):
    degree_s = scale_note % len(scale_bimap)
    degree_c = scale_bimap.s2c[degree_s]
    return 12 * (scale_note // len(scale_bimap)) + degree_c + root_c


def make_step_scaler(input_scale, input_root, output_scale, output_root):
    input_scale_bimap = ScaleBimap(input_scale)

    if output_scale == Scale.CHROMATIC:
        def step_scaler(step):
            chromatic_value = scale_to_chromatic(step.note.value, input_scale_bimap, input_root)
            step.note.value = chromatic_value
            return step
        return step_scaler

    output_scale_bimap = ScaleBimap(output_scale)

    def step_scaler(step):
        chromatic_value = scale_to_chromatic(step.note.value, input_scale_bimap, input_root)
        output_scale_value = chromatic_to_scale(chromatic_value, output_scale_bimap, output_root)
        step.note.value = output_scale_value

        return step

    return step_scaler
