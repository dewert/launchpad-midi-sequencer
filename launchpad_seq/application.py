import asyncio
import logging
import sys

import mido

from .configurator import LaunchpadSeqConfig, ClockSource
from .hardware import ClockEvent, get_port_pair, HardwareNotFound, clock_stream, get_output_port
from .sequencer import Sequencer
from .sequencercontroller import SequencerController
from .sequencerview import SequencerView

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Ports:
    launchpad_input_port: mido.ports.IOPort = None
    display_output_port: mido.ports.IOPort = None
    clock_input_port: mido.ports.IOPort = None
    note_output_port: mido.ports.IOPort = None


async def internal_clock_stream(bpm, ppqn):
    sleep_time = 60.0 / bpm / ppqn

    while True:
        await asyncio.sleep(sleep_time)
        yield ClockEvent.CLOCK


class SequencerApplication:
    def __init__(self, config_file):
        self.config = LaunchpadSeqConfig(config_file)

        self.ports = Ports()
        try:
            (
                self.ports.launchpad_input_port,
                self.ports.display_output_port,
            ) = get_port_pair(self.config.grid_device)

            if self.config.clock_source == ClockSource.EXTERNAL:
                self.ports.clock_input_port, self.ports.note_output_port = get_port_pair(
                    self.config.instrument_device
                )
            else:
                self.ports.note_output_port = get_output_port(self.config.instrument_device)

        except HardwareNotFound as e:
            logger.error("Could not find midi device '%s'", e.device)
            sys.exit(1)

        self.sequencer = Sequencer(ppqn=self.config.ppqn)
        self.view = SequencerView(self.ports.display_output_port)
        self.view.set_all()
        self.controller = SequencerController(self.sequencer, self.view)

    def clock_stream(self):
        if self.config.clock_source == ClockSource.EXTERNAL:
            return clock_stream(self.ports.clock_input_port)
        else:
            return internal_clock_stream(self.config.internal_bpm, self.config.ppqn)
