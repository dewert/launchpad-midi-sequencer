# coding: utf-8
import configparser
import logging
from enum import Enum
from typing import List

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ClockSource(Enum):
    INTERNAL = "internal"
    EXTERNAL = "external"


class LaunchpadSeqConfig:
    grid_device = None
    instrument_device = None
    internal_bpm = 120
    clock_source = ClockSource.INTERNAL
    ppqn = 24
    channels: List[int] = None

    def __init__(self, config_filename="config.ini"):
        config = configparser.ConfigParser()
        config.read(config_filename)
        self.ppqn = config.getint("General", "PPQN", fallback=24)
        self.internal_bpm = config.getint("General", "InternalBpm", fallback=120)
        self.clock_source = ClockSource(config.get("General", "ClockSource", fallback="external"))

        self.grid_device = config.get("Grid", "MidiIdentifier", fallback="launchpad")
        self.instrument_device = config.get("Instrument", "MidiIdentifier", fallback="mothman")

        self.channels = [
            config.getint("Sequencer", "Channel0", fallback=0),
            config.getint("Sequencer", "Channel1", fallback=1),
            config.getint("Sequencer", "Channel2", fallback=2),
            config.getint("Sequencer", "Channel3", fallback=3)
        ]
