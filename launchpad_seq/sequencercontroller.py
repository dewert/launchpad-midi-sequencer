# coding: utf-8
import logging
from enum import Enum

from .launchpad import GridValue, Coord
from .scale import Scale
from .sequencer import Sequencer
from .sequencerframe import Frameshift, FrameMode, SequencerFrame
from .sequencerview import SequencerView

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def null_handler(_sequencer, _frame, _view, grid_value: GridValue) -> None:
    logger.info("No handler defined for %s", grid_value.coord)


def pattern_switch_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    frame.track = grid_value.coord.row - 1
    view.update(sequencer, frame)


def step_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if not grid_value.pressed:
        return

    logger.info("Keypress on %d, %d", grid_value.coord.col, grid_value.coord.row)

    sequencer_step = frame.step[0] + grid_value.coord.col
    sequencer_pitch = frame.pitch[0] + 8 - grid_value.coord.row

    logger.info(
        "Toggling pattern %d, step %d, pitch %d",
        frame.track,
        sequencer_step,
        sequencer_pitch,
    )

    step = sequencer.toggle_step(frame.track, sequencer_step, sequencer_pitch)

    logger.debug("Sequencer toggle returned %s", step.gate)
    view.update(sequencer, frame)


def frame_nav_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if not grid_value.pressed:
        return

    col = grid_value.coord.col
    direction = {
        0: Frameshift.UP,
        1: Frameshift.DOWN,
        2: Frameshift.LEFT,
        3: Frameshift.RIGHT,
    }

    try:
        frame.shift(direction[col], sequencer)
    except KeyError:
        logger.debug("Can't handle %s as frame navigation", grid_value.coord)
        return

    view.update(sequencer, frame)


def track_denom_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if not grid_value.pressed:
        return

    denom = 2 ** (grid_value.coord.row - 3)
    sequencer.set_track_denom(frame.track, denom)
    view.update(sequencer, frame)


def length_mode_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if grid_value.pressed:
        frame.enter_mode(FrameMode.LENGTH)
    else:
        frame.exit_mode(FrameMode.LENGTH)

    view.update(sequencer, frame)


def length_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if grid_value.coord.row > 4:
        return

    sequencer.set_length(grid_value.coord.row - 1, (grid_value.coord.col + 1) * 8)
    view.update(sequencer, frame)


def scale_mode_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue,
) -> None:
    if grid_value.pressed:
        frame.enter_mode(FrameMode.SCALE)
    else:
        frame.exit_mode(FrameMode.SCALE)

    view.update(sequencer, frame)


note_names = {
    "C": 0,
    "C#": 1,
    "D": 2,
    "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6,
    "G": 7,
    "G#": 8,
    "A": 9,
    "Bb": 10,
    "B": 11,
}


def scale_select_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue
) -> None:
    if not grid_value.pressed:
        return

    if 1 <= grid_value.coord.row <= 4:
        try:
            scale = frame.scale_mappings[grid_value.coord]
            logger.info("Setting scale to %s", scale)
        except KeyError:
            logger.debug("No scale value registered at %s", grid_value.coord)
            return

        sequencer.set_scale(scale=scale)
        frame.reframe(len(scale.value))

    elif 5 <= grid_value.coord.row <= 8:
        sequencer.set_scale(root=frame.note_mappings[grid_value.coord])

    view.update(sequencer, frame)


def start_stop_handler(
    sequencer: Sequencer,
    frame: SequencerFrame,
    view: SequencerView,
    grid_value: GridValue
) -> None:
    if not grid_value.pressed:
        return

    sequencer.toggle()
    view.update(sequencer, frame)


class SequencerController:
    def __init__(self, sequencer: Sequencer, view: SequencerView):
        self._sequencer = sequencer
        self._view = view
        self._sequencer_frame = SequencerFrame()

    def handle_press(self, grid_value: GridValue):
        handler_args = self._sequencer, self._sequencer_frame, self._view, grid_value

        if (grid_value.coord.col < 8) and (grid_value.coord.row > 0):
            if self._sequencer_frame.mode == FrameMode.CONTENT:
                step_handler(*handler_args)
            elif self._sequencer_frame.mode == FrameMode.LENGTH:
                length_handler(*handler_args)
            elif self._sequencer_frame.mode == FrameMode.SCALE:
                scale_select_handler(*handler_args)
        elif (0 <= grid_value.coord.col < 4) and (grid_value.coord.row == 0):
            frame_nav_handler(*handler_args)
        elif (0 < grid_value.coord.row < 5) and (grid_value.coord.col == 8):
            pattern_switch_handler(*handler_args)
        elif (4 < grid_value.coord.row < 9) and (grid_value.coord.col == 8):
            track_denom_handler(*handler_args)
        elif grid_value.coord == Coord(6, 0):
            length_mode_handler(*handler_args)
        elif grid_value.coord == Coord(5, 0):
            scale_mode_handler(*handler_args)
        elif grid_value.coord == Coord(7, 0):
            start_stop_handler(*handler_args)

    def update_step(self):
        self._view.update(self._sequencer, self._sequencer_frame)
