import asyncio
import logging
import time
from enum import Enum

import mido

LAUNCHPAD_IDENTIFIER = "launchpad"


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class HardwareNotFound(Exception):
    def __init__(self, device):
        self.device = device


def get_output_port(identifier=LAUNCHPAD_IDENTIFIER):
    try:
        output_name = next(
            filter(lambda x: identifier in x.lower(), mido.get_output_names())
        )
    except StopIteration:
        raise HardwareNotFound(identifier)

    output_port = mido.open_output(output_name)
    return output_port


def get_input_port(identifier=LAUNCHPAD_IDENTIFIER):
    try:
        input_name = next(
            filter(lambda x: identifier in x.lower(), mido.get_input_names())
        )
    except StopIteration:
        raise HardwareNotFound(identifier)

    input_port = mido.open_input(input_name)
    return input_port


def get_port_pair(identifier):
    return get_input_port(identifier), get_output_port(identifier)


class ClockEvent(Enum):
    NONE = False
    CLOCK = "clock"
    START = "start"
    RESET = "reset"


def check_for_clock(input_port: mido.ports.IOPort) -> ClockEvent:
    msg = input_port.poll()
    if msg is None:
        return ClockEvent.NONE

    try:
        return ClockEvent(msg.type)
    except ValueError:
        return ClockEvent.NONE


async def clock_stream(input_port: mido.ports.IOPort):
    async for msg in get_input_stream(input_port):
        try:
            yield ClockEvent(msg.type)
        except ValueError:
            pass


def get_input_stream(input_port):
    loop = asyncio.get_event_loop()
    queue = asyncio.Queue()

    def callback(message):
        loop.call_soon_threadsafe(queue.put_nowait, message)

    async def stream():
        while True:
            yield await queue.get()

    input_port.callback = callback

    return stream()
