# coding: utf-8
import logging
from typing import List

from .scale import Scale, make_step_scaler

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Note:
    def __init__(self, value: int = 0):
        self.value = value

    def clone(self):
        return Note(self.value)


class Step:
    def __init__(self):
        self.note = Note()
        self.gate = False
        self.track = None

    def clone(self):
        clone = Step()
        clone.note = self.note.clone()
        clone.gate = self.gate
        return clone


class Track:
    num_steps = 16
    clocks = -1
    current_step_count = -1
    step_returned = False

    def __init__(self, ppqn=24):
        self.steps = [Step() for _ in range(self.num_steps)]
        self.clock_div = ppqn

    def clock(self):
        self.clocks += 1
        if self.clocks % self.clock_div == 0:
            self.step_returned = False
            self.clocks = 0
            self.current_step_count += 1
            assert not self.current_step_count > self.num_steps
            if self.current_step_count == self.num_steps:
                self.current_step_count = 0

    def next_step(self):
        if self.step_returned:
            return None

        self.step_returned = True
        return self.steps[self.current_step_count].clone()

    def time_reset(self):
        self.clocks = -1
        self.current_step_count = -1
        self.step_returned = False

    def set_div(self, div: int):
        self.clock_div = div

    def __getitem__(self, item: int):
        return self.steps[item]

    def set_length(self, length):
        self.num_steps = length
        logger.debug("Num steps set to %d", self.num_steps)
        logger.debug("Current step: %d", self.current_step_count)

        if self.current_step_count >= self.num_steps:
            self.current_step_count %= 8

        steps_len = len(self.steps)
        if steps_len < self.num_steps:
            self.steps.extend([Step() for _ in range(steps_len, self.num_steps + 1)])


class Sequencer:
    def __init__(self, ppqn=24, scale=Scale.CHROMATIC, root=0):
        self.ppqn = ppqn
        self.scale = None
        self.root = None
        self._output_scaler = None
        self.set_scale(scale, root)
        self.tracks = [Track(ppqn=ppqn) for _ in range(4)]
        self.running = False

    def set_scale(self, scale=None, root=None):
        if scale is None and root is None:
            return

        if scale is None:
            scale = self.scale
        if root is None:
            root = self.root

        self.scale = scale
        self.root = root
        self._output_scaler = make_step_scaler(scale, root, Scale.CHROMATIC, 0)

    def clock_steps(self):
        if not self.running:
            return

        for track in self.tracks:
            track.clock()

        yield from self._steps()

    def _steps(self) -> List[Step]:
        for track_num, track in enumerate(self.tracks):
            step = track.next_step()

            if step is not None and step.gate > 0:
                step.track = track_num
                yield self._output_scaler(step)

    def toggle_step(self, track: int, step: int, pitch: int) -> Step:
        if self[track][step].note.value == pitch:
            self[track][step].gate = not self[track][step].gate
        else:
            self[track][step].note = Note(pitch)
            self[track][step].gate = True

        return self[track][step]

    def set_track_denom(self, track: int, div: int):
        self[track].set_div(self.ppqn // (div // 4))

    def get_track_denom(self, track: int):
        return self.ppqn / self[track].clock_div * 4

    def time_reset(self):
        for track in self.tracks:
            track.time_reset()

    def __getitem__(self, item: int):
        return self.tracks[item]

    def set_length(self, track, length):
        self[track].set_length(length)

    def visit_steps(self, step_visitor):
        for track in self.tracks:
            for i in range(len(track.steps)):
                track.steps[i] = step_visitor(track.steps[i])

    def toggle(self):
        self.running = not self.running
