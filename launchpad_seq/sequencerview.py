# coding: utf-8

import logging
import math

from .launchpad import Coord, Colour, set_led
from .sequencer import Step
from .sequencerframe import FrameMode

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SequencerView:
    def __init__(self, port):
        self._port = port
        self._shadow_grid = [[Colour.OFF] * 9 for _ in range(9)]

    def update(self, sequencer, frame):
        self.update_step_indicators(frame, sequencer)
        self.update_track_info(frame, sequencer)
        if frame.mode == FrameMode.CONTENT:
            self.update_pattern_content(frame, sequencer)
        elif frame.mode == FrameMode.LENGTH:
            self.update_pattern_length(sequencer)
        elif frame.mode == FrameMode.SCALE:
            self.update_scale(frame, sequencer)

    def update_pattern_length(self, sequencer):
        for row in range(1, 5):
            for col in range(0, 8):
                self.update_coord(Coord(col, row), col < sequencer[row - 1].num_steps // 8)

        for row in range(5, 9):
            for col in range(0, 8):
                self.update_coord(Coord(col, row), False)

    def update_step_indicators(self, frame, sequencer):
        # Show current step and segment
        step_count = sequencer[frame.track].current_step_count
        for col in range(8):
            current_step = (step_count in range(frame.step[0], frame.step[1] + 1)) and (
                    col == step_count % 8
            )
            current_segment = col == step_count // 8
            editing_segment = col == frame.step[0] // 8

            if editing_segment:
                if current_segment:
                    colour = Colour.RED_HIGH
                else:
                    colour = Colour.RED_LOW
            elif current_segment and not current_step:
                colour = Colour.AMBER_LOW
            else:
                colour = Colour.AMBER_HIGH

            self.update_coord(
                Coord(col, 0),
                current_step or current_segment or editing_segment,
                colour,
            )

    def update_pattern_content(self, frame, sequencer):
        # Show current pattern content
        for step in range(frame.step[0], frame.step[1] + 1):
            step_val: Step = sequencer[frame.track][step]
            for pitch in range(frame.pitch[0], frame.pitch[1] + 1):
                self.update_coord(
                    Coord(step - frame.step[0], 8 - (pitch - frame.pitch[0])),
                    step_val.gate and step_val.note.value == pitch,
                )

    def update_track_info(self, frame, sequencer):
        # Show current track
        colour = Colour.GREEN_HIGH if sequencer.running else Colour.RED_HIGH
        for row in range(1, 5):
            self.update_coord(Coord(8, row), row - 1 == frame.track, colour)
        # Show current track time division
        for row in range(5, 9):
            set_row = math.log2(sequencer.get_track_denom(frame.track)) + 3
            self.update_coord(Coord(8, row), row == set_row)

    def update_scale(self, frame, sequencer):
        # Show current scale
        for row in range(1, 5):
            for col in range(8):
                coord = Coord(col, row)
                colour = Colour.RED_HIGH if frame.scale_mappings.get(coord, None) == sequencer.scale else Colour.AMBER_HIGH
                self.update_coord(coord, coord in frame.scale_mappings, colour)

        for row in range(7, 9):
            for col in range(8):
                coord = Coord(col, row)
                colour = Colour.GREEN_HIGH if row == 7 else Colour.YELLOW
                self.update_coord(coord, coord in frame.note_mappings, colour)

    def update_coord(self, coord: Coord, lit: bool, colour=Colour.AMBER_HIGH):
        if (self._shadow_grid[coord.col][coord.row] != Colour.OFF) != lit or (
            lit and self._shadow_grid[coord.col][coord.row] != colour
        ):
            logger.debug("Updating row %d col %d to %s", coord.row, coord.col, lit)
            self._shadow_grid[coord.col][coord.row] = colour if lit else Colour.OFF
            set_led(self._port, coord, self._shadow_grid[coord.col][coord.row])

    def set_all(self):
        for col, values in enumerate(self._shadow_grid):
            for row, colour in enumerate(values):
                set_led(self._port, Coord(col, row), colour)
