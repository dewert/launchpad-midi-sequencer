import logging
from collections import namedtuple
from enum import Enum
from time import sleep
from typing import Union

import mido
from mido import parse

from .hardware import get_output_port, get_input_stream

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

Coord = namedtuple("Coord", ["col", "row"])
GridValue = namedtuple("GridValue", ["pressed", "coord"])


class GridMap(Enum):
    UP = Coord(0, 0)
    DOWN = Coord(1, 0)
    LEFT = Coord(2, 0)
    RIGHT = Coord(3, 0)
    SESSION = Coord(4, 0)
    USER_1 = Coord(5, 0)
    USER_2 = Coord(6, 0)
    MIXER = Coord(7, 0)
    VOL = Coord(8, 1)
    PAN = Coord(8, 2)
    SND_A = Coord(8, 3)
    SND_B = Coord(8, 4)
    STOP = Coord(8, 5)
    TRK_ON = Coord(8, 6)
    SOLO = Coord(8, 7)
    ARM = Coord(8, 8)


class Colour(Enum):
    OFF = 0x0C
    RED_LOW = 0x0D
    RED_HIGH = 0x0F
    AMBER_LOW = 0x1D
    AMBER_HIGH = 0x3F
    YELLOW = 0x3E
    GREEN_LOW = 0x1C
    GREEN_HIGH = 0x3C


def set_led(port, coord: Coord, colour: Colour = Colour.YELLOW):
    assert 0 <= coord.col < 9
    assert 0 <= coord.row < 9

    if isinstance(colour, Colour):
        colour_value = colour.value
    else:
        colour_value = colour

    if coord.row > 0:
        msg = parse([0x90, 0x10 * (coord.row - 1) + coord.col, colour_value])
    else:
        msg = parse([0xB0, 0x68 + coord.col, colour_value])

    port.send(msg)


def parse_to_grid(msg: mido.Message) -> Union[GridValue, None]:
    msg_bytes = msg.bytes()

    if msg_bytes[0] == 0xB0:
        row = 0
        col = msg_bytes[1] - 0x68
        return GridValue(msg_bytes[2] == 127, Coord(col, row))

    if msg_bytes[0] == 0x90:
        col = 0x0F & msg_bytes[1]
        row = ((0xF0 & msg_bytes[1]) >> 4) + 1
        return GridValue(msg_bytes[2] == 127, Coord(col, row))

    return None


def test_pattern():
    output_port = get_output_port()

    for colour in Colour:
        for row in range(9):
            for col in range(9):
                set_led(output_port, Coord(col, row), colour)
                sleep(0.01)

    for row in range(9):
        for col in range(9):
            set_led(output_port, Coord(col, row), Colour.OFF)
            sleep(0.01)


def get_pending_input(input_port):
    for msg in input_port.iter_pending():
        result = parse_to_grid(msg)
        if result:
            yield result


async def press_stream(input_port):
    async for msg in get_input_stream(input_port):
        result = parse_to_grid(msg)
        if result:
            yield result
