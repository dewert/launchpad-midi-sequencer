import logging
from enum import Enum

from .scale import Scale
from .sequencer import Sequencer


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

MIN_MIDI_PITCH = 0
MAX_MIDI_PITCH = 127


class Frameshift(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3


class FrameMode(Enum):
    CONTENT = 0
    LENGTH = 1
    SCALE = 2


class SequencerFrame:
    def __init__(self):
        self.track = 0
        self.pitch_shift_size = 8
        self.pitch = (36, 43)
        self.step = (0, 7)
        self.mode = FrameMode.CONTENT
        self.scale_mappings = {
            (0, 1): Scale.CHROMATIC,
            (1, 1): Scale.MAJOR,
            (2, 1): Scale.MINOR,
            (3, 1): Scale.MIXOLYDIAN,
            (4, 1): Scale.BLUES
        }
        self.note_mappings = {
            (0, 8): 0,
            (1, 7): 1,
            (1, 8): 2,
            (2, 7): 3,
            (2, 8): 4,
            (3, 8): 5,
            (4, 7): 6,
            (4, 8): 7,
            (5, 7): 8,
            (5, 8): 9,
            (6, 7): 10,
            (6, 8): 11,
            (7, 8): 0
        }

    def enter_mode(self, mode: FrameMode):
        if self.mode == FrameMode.CONTENT:
            self.mode = mode
            return True
        return False

    def exit_mode(self, mode: FrameMode):
        if self.mode == mode:
            self.mode = FrameMode.CONTENT
            return True
        return False

    def reframe(self, pitch_shift_size):
        base_pitch = self.pitch[0] - (self.pitch[0] % pitch_shift_size)
        new_low_pitch, new_high_pitch = (base_pitch, base_pitch + 7)
        if new_high_pitch > MAX_MIDI_PITCH or new_low_pitch < MIN_MIDI_PITCH:
            logger.debug(
                "Frame shift out of range - pitches would be (%d, %d)",
                new_low_pitch,
                new_high_pitch,
            )
            return

        self.pitch_shift_size = min(pitch_shift_size, 8)
        self.pitch = (new_low_pitch, new_high_pitch)

    def shift(self, direction: Frameshift, sequencer: Sequencer):
        pitch_change = 0
        step_change = 0

        if direction == Frameshift.UP:
            pitch_change = self.pitch_shift_size
        elif direction == Frameshift.DOWN:
            pitch_change = -self.pitch_shift_size
        elif direction == Frameshift.LEFT:
            step_change = -8
        elif direction == Frameshift.RIGHT:
            step_change = 8

        new_max_step = self.step[1] + step_change
        new_min_step = self.step[0] + step_change
        if sequencer[self.track].num_steps < new_max_step or new_min_step < 0:
            logger.debug(
                "Frame shift out of range - steps would be (%d, %d)",
                new_min_step,
                new_max_step,
            )
        else:
            self.step = (new_min_step, new_max_step)

        new_low_pitch = self.pitch[0] + pitch_change
        new_high_pitch = self.pitch[1] + pitch_change

        if new_high_pitch > MAX_MIDI_PITCH or new_low_pitch < MIN_MIDI_PITCH:
            logger.debug(
                "Frame shift out of range - pitches would be (%d, %d)",
                new_low_pitch,
                new_high_pitch,
            )
        else:
            self.pitch = (new_low_pitch, new_high_pitch)
